class Category < ActiveRecord::Base
  has_many :topics

  def self.get_project_name
    "FORUM"
  end

end
