class TopicsController < ApplicationController

  def new
    @topic = Topic.new
  end

  def create
    @topic = Topic.new(topic_params)
    if @topic.save!
      render :show
    else
      render :new
    end
  end

  def show
    @topic ||= Topic.find(params[:id])
    @post = Post.new
  end

  private

  def topic_params
    params.require(:topic).permit(:name, :category_id)
  end

end
