class PostsController < ApplicationController
  def create
    @post = Post.new(post_params)
    @topic = Topic.find(@post.topic_id)

    @post.save!
    redirect_to @topic
  end

  private

  def post_params
    params.require(:post).permit(:content, :user_id, :topic_id)
  end
end
